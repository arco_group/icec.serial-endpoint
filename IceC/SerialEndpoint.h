/* -*- mode: c; coding: utf-8 -*- */

#ifndef _SERIALENDPOINT_ENDPOINT_H_
#define _SERIALENDPOINT_ENDPOINT_H_

#include <IceC/IceC.h>

#ifdef __cplusplus
extern "C" {
#endif

#define Ice_EndpointTypeSerial 12

bool SerialEndpoint_getProtocolType(const char* proto, Ice_EndpointType* result);
bool SerialEndpoint_InputStream_init(Ice_InputStreamPtr self, int fd, Ice_EndpointType type);
bool SerialEndpoint_ObjectAdapter_activate(Ice_ObjectAdapterPtr self);
bool SerialEndpoint_EndpointInfo_init(Ice_EndpointInfoPtr self,
                                      Ice_EndpointType type,
                                      const char* endpoint);

bool SerialEndpoint_ObjectPrx_connect(Ice_ObjectPrxPtr self);
bool SerialEndpoint_ObjectPrx_init(Ice_ObjectPrxPtr self,
                                   Ice_EndpointType type,
                                   const char* strprx);

bool SerialEndpoint_Connection_send(Ice_ConnectionPtr self, int fd, byte* data,
                                    uint16_t* size);

/* plugin specific functions */
void SerialEndpoint_init(Ice_CommunicatorPtr ic);

#ifdef __cplusplus
}
#endif

#endif /* _SERIALENDPOINT_ENDPOINT_H_ */
